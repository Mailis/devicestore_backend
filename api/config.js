const config = {
    PORT:5000,
    PORT_TEST:5001,

    NODE_ENV_DEV:"dev",
    NODE_ENV_TEST:"test",
    NODE_ENV_PROD:"prod",

    HOST:"http://localhost",

    DEVELOPMENT_DATABASE_URL:"mongodb://localhost:27017/devicestore_dev",
    PRODUCTION_DATABASE_URL:"mongodb://localhost:27017/devicestore_prod",
    TEST_DATABASE_URL:"mongodb://localhost:27017/devicestore_test"
};

module.exports = config;