var ProductAttributeController = function(ProductAttributes){

    //read all attributes
    var getAllAttributes = async (req, res) => {
        const attrs = await ProductAttributes.find();
        res.send(attrs);
    }

    //create an attribute
    var createAttribute = async (req, res) => {
        const attrs = new ProductAttributes({
            color: req.body.color,
        })
        await attrs.save()
        res.send(attrs)
    }


    return{
        getAllAttributes,
        createAttribute
    }
}

module.exports = ProductAttributeController;
