var ProductController = function(Product){

    //read all products
    var getAllProducts = async (req, res) => {
        const products = await Product.find().populate('color');
        res.send(products);
    }

    //create a product
    var createProduct = async (req, res) => {
        if(!req.body.title){
            res.status(400);
            res.send('Title is required!');
        }
        else{
            const product = new Product({
                title: req.body.title,
                description: req.body.description
            })
            await product.save()
            res.status(201);
            res.send(product)
        }
    }


    return{
        getAllProducts,
        createProduct
    }
}

module.exports = ProductController;
