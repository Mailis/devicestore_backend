const { listeners } = require("../models/Store");

var StoreController = function(Store){

    //read all products
    var getProductsList = async (req, res) => {
        const productList = await Store.find().populate({ 
            path: 'product',
            populate: {
              path: 'color',
              model: 'ProductAttributes'
            } 
         });
        res.send(productList);
    }

    var modifyQuantity = async (req, res, increase) => {
        let storeid = req.params.storeId;
        let amount_abs = req.params.amount;
        let storeitem =  await Store.findById(storeid);
        let quantity = storeitem.quantity;
        let opIsAvailable = true;
        if(increase === false){//decreasing amount
            if(quantity < amount_abs){
                opIsAvailable = false;
                res.statusCode = 404;
                res.json("Product is not available");
            }
        }

        if(opIsAvailable){
            let amount = increase? amount_abs : amount_abs*-1;
            var conditions = { _id: storeid }, 
                update = { $inc: { quantity: amount }},
                options = { new: true };
            const data = await Store.updateOne(conditions, update, options);
            res.json(data);
        }
    }

    var increaseQuantity = async (req, res) => {
        modifyQuantity(req, res, true);
    }

    var decreaseQuantity = async (req, res) => {
        modifyQuantity(req, res, false);
    }


    return{
        getProductsList,
        increaseQuantity,
        decreaseQuantity
    }
}

module.exports = StoreController;
