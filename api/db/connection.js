const mongoose = require('mongoose');
const config = require('../config');
const createDevData = require("../fakedata/dataGenerator");

const port = config.PORT;
const env = config.NODE_ENV;
const db_dev = config.DEVELOPMENT_DATABASE_URL;
const db_test = config.TEST_DATABASE_URL;
const db_prod = config.PRODUCTION_DATABASE_URL;

var connect = function(environment){
    //console.log("ENVIRONM: ", environment);
    var mongoDB = db_dev;
    if(environment === config.NODE_ENV_TEST){
        mongoDB = db_test;
    }
    else if(environment === config.NODE_ENV_PROD){
        mongoDB = db_prod;
    }
    // Connect to MongoDB database
    //Set up default mongoose connection
    mongoose.connect(mongoDB, { 
        useCreateIndex: true,
        useUnifiedTopology: true,
        useNewUrlParser: true });

    //Get the default connection
    var db = mongoose.connection;

    //Bind connection to error event (to get notification of connection errors)
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    //make development dummy data
    if(environment === config.NODE_ENV_DEV){
       createDevData();
    }
}
//mongoDB graceful exit
const disconnect = () => mongoose.connection.close();
module.exports = {connect, disconnect};
