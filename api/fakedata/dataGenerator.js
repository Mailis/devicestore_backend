const mongoose = require('mongoose');
const dummy = require('mongoose-dummy');
const ignoredFields = ['_id','created_at', '__v', /detail.*_info/];
const Product = require("../models/Product");
const ProductAttribute = require("../models/ProductAttributes");
const Store = require("../models/Store");

const createDevData = () =>{
    ProductAttribute.deleteMany(function(err) {
        if(err) console.log(err);
        console.log("Successful deletion");
    });
    Product.deleteMany(function(err) {
        if(err) console.log(err);
        console.log("Successful deletion");
    });

    Store.deleteMany(function(err) {
        if(err) console.log(err);
        console.log("Successful deletion");
    });

    mongoose.set('useFindAndModify', false);
    const productColors = ["red", "black", "yellow", "green", "white"];
    const productNames = ["iPhone", "Galaxy S", "Samsung TV", "iPad", "CanoScan Lide 600"];
    const productQuantities = [100, 10, 0, 5, 99];
    const productPrices = [101.99, 1.99, 799.90, 115.20, 199.45];
    const productDescriptions = [
        "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. ", 
        "Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. ", 
        "Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.", 
        "Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?", 
        "Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem."];


    productColors.forEach((c, i) => {
        var pa = new ProductAttribute(
            { color:c }
        );
        pa.save();

        var p = new Product(
            { 
                title: productNames[i],
                color: pa._id,
                description: productDescriptions[i]
            }
        );
        p.save();


        var store = new Store(
            {
                product: p._id,
                quantity: productQuantities[i],
                price: productPrices[i]
            }
        );
        store.save();
    });
}

module.exports = createDevData;