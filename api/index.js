const express = require("express");
const cors = require('cors');
const winston = require('winston')
const consoleTransport = new winston.transports.Console()
const myWinstonOptions = {
    transports: [consoleTransport]
}
const logger = new winston.createLogger(myWinstonOptions)

const Product = require("./models/Product");
const ProductAttribute  = require("./models/ProductAttributes");
const Store  = require("./models/Store");

const config = require('./config');
const port = config.PORT;

const {connect} = require("./db/connection");
connect(config.NODE_ENV_DEV);

const app = express();
app.use(cors());
app.use(express.json());

function logRequest(req, res, next) {
    logger.info(req.url)
    next()
}
app.use(logRequest)

function logError(err, req, res, next) {
    logger.error(err)
    next()
}
app.use(logError)



var productRouter = require('./routes/productRoutes')(Product);
app.use("/api", productRouter);
var productAttributeRouter = require('./routes/productAttributeRoutes')(ProductAttribute);
app.use("/api", productAttributeRouter);
var storeRouter = require('./routes/storeRoutes')(Store);
app.use("/api", storeRouter);

app.listen(port, () => {
	console.log("Server has started! " + config.HOST+":"+config.PORT)
});
