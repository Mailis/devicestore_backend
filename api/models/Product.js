const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ProductAttributes = require('./ProductAttributes');


var ProductSchema = new Schema(
  {
    title: {
      type: String,
      unique: true,
      index: true,
      required: 'Enter the name of a product'
    },
    description: {
      type: String,
      default: ""
    },
    color: {
      type: Schema.Types.ObjectId,
      ref: "ProductAttribute"
    }
  },
  {timestamps: true}
);


module.exports = mongoose.model('Product', ProductSchema);