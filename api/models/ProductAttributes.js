const mongoose = require('mongoose');
const Schema = mongoose.Schema;


var ProductAttributeSchema = new Schema(
  {
    color: {
      type: String,
      index: true,
      unique: true,
      lowercase: true
    }
  }
);

module.exports = mongoose.model('ProductAttribute', ProductAttributeSchema);