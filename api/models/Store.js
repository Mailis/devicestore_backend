const mongoose = require('mongoose');
const Schema = mongoose.Schema;


var StoreSchema = new Schema(
    {
        product: {
            type: Schema.Types.ObjectId,
            ref: "Product",
            index: true,
            required: true
        },
        quantity: {
            type: Number,
            min: 0,
            default: 0
        },
        price: {
            type: Number,
            required: true,
            min: 0.0,
            default: 0.0
        }
    },
    {timestamps: true}
);

module.exports = mongoose.model('Store', StoreSchema);