const express = require("express");

var productAttributeRoutes = function(ProductAttribute){
	const router = express.Router();
	const ProductAttributeController = require("../controllers/ProductAttributeController")(ProductAttribute);
	router.route('/attributes')
		.get(ProductAttributeController.getAllAttributes)
		.post(ProductAttributeController.createAttribute);
	return router;
}

module.exports = productAttributeRoutes;