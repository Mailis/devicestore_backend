const express = require("express");

var productRoutes = function(Product){
	const router = express.Router();
	const ProductController = require("../controllers/ProductController")(Product);
	router.route('/products')
		.get(ProductController.getAllProducts)
		.post(ProductController.createProduct);
	return router;
}
// Get all products
// router.get("/products", async (req, res) => {
// 	// const products = await Product.find();
// 	// res.send(products);
// 	await ProductController.getAllProducts(req, res);
// });

//create a product
// router.post("/products", async (req, res) => {
// 	const product = new Product({
// 		title: req.body.title,
// 		description: req.body.description,
// 	})
// 	await product.save()
// 	res.send(product)
// })



module.exports = productRoutes;