const express = require("express");

var storeRoutes = function(Store){
	const router = express.Router();
    const StoreController = require("../controllers/StoreController")(Store);
    
	router.route('/store')
        .get(StoreController.getProductsList);
        
    router.route('/store/increase/:amount/:storeId')
        .put(StoreController.increaseQuantity);

    router.route('/store/decrease/:amount/:storeId')
        .put(StoreController.decreaseQuantity);

	return router;
}


module.exports = storeRoutes;