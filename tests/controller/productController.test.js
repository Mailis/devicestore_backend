var should = require('should'),
    sinon = require('sinon');
const Product = require('../../api/models/Product');
const productController = require('../../api/controllers/ProductController')(Product);

describe('Product Controller Tests:', function(){
    // req contains unchanged body and id parameters
    // required for all tests
    let req = { 
        body: { // for testing create vehicle
            title: "Samsung ghT3-11",
            description: "Your friend at times",
            color: "RTYU7rCVBN77ihb9ufeb9ufU)UBH",
        },
        params: { 
            id: "5aa06b999b80752cfd536fdc" // for testing get, delete and update product
        }
    };


    describe('Post', function(){
        beforeEach(function () {
            res = {
                json: sinon.spy(),
                status: sinon.stub().returns({ end: sinon.spy() }),
                send: sinon.spy()
            };
        });

        it('should not allow an empty title on post', function(){
            req.body.title = "";
            productController.createProduct(req, res);
            res.status.calledWith(400).should.equal(true, 'Bad satatus ' + res.status);
            res.send.calledWith('Title is required!').should.equal(true);
        });

    })
})