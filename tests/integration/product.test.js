var should = require('should'),
    request = require('supertest'),
    app = require('./testapp.js'),
    mongoose = require('mongoose'),
    Product = mongoose.model('Product'),
    agent = request.agent(app);
    
const { assert } = require('sinon');
const {disconnect} = require("../../api/db/connection");

describe('Product Crud Test', function(){
    var product = {
        title: "Samsung ghT3-11",
        description: "Your friend at times"
    };
    let createdProduct;

    beforeAll(async () => {
        createdProduct = product;
    });

    it('Should allow a product to be posted and return a title, description and _id', done => {
        agent.post('/api/products')
            .send(createdProduct)
            .expect(201)
            .expect("Content-Type", /json/)
            .end(function(err, results){
                results.body.title.should.equal(createdProduct.title);
                results.body.description.should.equal(createdProduct.description);
                results.body.should.have.property('_id');
                done();
            })
    })

    it("should get all products", done => {
        agent.get("/api/products")
          .expect("Content-Type", /json/)
          .expect(200)
          .end(function(err, results){
              //console.log("GET: ", results.body);
              results.body.length.should.equal(1);
              results.body[0].title.should.equal(createdProduct.title);
              results.body[0].description.should.equal(createdProduct.description);
              done();
          })
    });

    afterEach(function(done){
        done();
    })

    afterAll(async () => { 
        await Product.deleteMany().exec();
        await disconnect();
    })
});