var should = require('should'),
    request = require('supertest'),
    app = require('./testapp.js'),
    mongoose = require('mongoose'),
    ProductAttribute = mongoose.model('ProductAttribute'),
    agent = request.agent(app);
    
const { assert } = require('sinon');
const {disconnect} = require("../../api/db/connection");

describe('ProductAttribute CRUD Test', function(){
    var productAttribute = {
        color: "YelLow"
    };
    let createdProductAttribute;
    let colorToLowerCase = productAttribute.color.toLowerCase();

    beforeAll(async () => {
        createdProductAttribute = productAttribute;
    });

    it('Should allow a productAttribute to be posted and return a lowercase color attr. and _id', done => {
        agent.post('/api/attributes')
            .send(createdProductAttribute)
            .expect(201)
            .expect("Content-Type", /json/)
            .end(function(err, results){
                results.body.color.should.equal(colorToLowerCase);
                results.body.should.have.property('_id');
                done();
            })
    })

    it("should get all productAttributes", done => {
        agent.get("/api/attributes")
          .expect("Content-Type", /json/)
          .expect(200)
          .end(function(err, results){
              //console.log("GET: ", results.body);
              results.body.length.should.equal(1);
              results.body[0].color.should.equal(colorToLowerCase);
              done();
          })
    });

    afterEach(function(done){
        done();
    })

    afterAll(async () => { 
        await ProductAttribute.deleteMany().exec();
        await disconnect();
    })
});