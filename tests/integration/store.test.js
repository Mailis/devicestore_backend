var should = require('should'),
    request = require('supertest'),
    app = require('./testapp.js'),
    mongoose = require('mongoose'),
    Product = mongoose.model('Product'),
    Store = mongoose.model('Store'),
    agent = request.agent(app);
    
const { assert } = require('sinon');
const {disconnect} = require("../../api/db/connection");

describe('STORE routes Test', function(){
    var product = {
        title: "Samsung ghT3-11",
        description: "Your friend at times"
    };
    
    let createdStrore;
    let prod;

    beforeAll(async () => {
        prod = new Product(product );
        prod.save();
        createdStrore = new Store({
            quantity: 10,
            price: 44.99,
            product: prod._id
        });
        createdStrore.save();
    });

    it("Should receive products list from store", done => {
        agent.get("/api/store")
          .expect("Content-Type", /json/)
          .expect(200)
          .end(function(err, results){
              results.body.length.should.equal(1);
              results.body[0].product._id.should.equal(prod._id.toString());
              results.body[0].product.title.should.equal(product.title);
              results.body[0].product.description.should.equal(product.description);
              results.body[0].price.should.equal(createdStrore.price);
              results.body[0].quantity.should.equal(createdStrore.quantity);
              done();
          })
    });

    it('Should decrease product amount in store by 10', done => {
        let storeId = createdStrore._id.toString();
        let amount = createdStrore.quantity;
        agent.put('/api/store/decrease/' + amount + '/' + storeId)
            .expect(200)
            .expect("Content-Type", /json/)
            .end(function(err, results){
                results.body.ok.should.equal(1);
                done();
            });
    })

    it('Should have quantity of 0 after decreasing', done => {
        agent.get("/api/store")
          .expect("Content-Type", /json/)
          .expect(200)
          .end(function(err, results){
              results.body.length.should.equal(1);
              results.body[0].product._id.should.equal(prod._id.toString());
              results.body[0].product.title.should.equal(product.title);
              results.body[0].product.description.should.equal(product.description);
              results.body[0].price.should.equal(createdStrore.price);
              results.body[0].quantity.should.equal(0);
              done();
          })
    });


    it('Should throw status not found when decreasing 0 product amount', done => {
        let storeId = createdStrore._id.toString();
        agent.put('/api/store/decrease/1/' + storeId)
            .expect(404)
            .expect("Content-Type", /json/)
            .end(function(err, results){
                //console.log("DECREASE UNAVAILABLE STORE: ", results.body);
                results.body.should.equal("Product is not available");
                done();
            });
    })

    it('Should increase product amount in store by 1', done => {
        let storeId = createdStrore._id.toString();
        agent.put('/api/store/increase/1/' + storeId)
            .expect(200)
            .expect("Content-Type", /json/)
            .end(function(err, results){
                results.body.ok.should.equal(1);
                done();
            });
    });

    it('Should have quantity of 1 after increasing', done => {
        agent.get("/api/store")
          .expect("Content-Type", /json/)
          .expect(200)
          .end(function(err, results){
              results.body.length.should.equal(1);
              results.body[0].product._id.should.equal(prod._id.toString());
              results.body[0].product.title.should.equal(product.title);
              results.body[0].product.description.should.equal(product.description);
              results.body[0].price.should.equal(createdStrore.price);
              results.body[0].quantity.should.equal(1);
              done();
          })
    })


    afterEach(function(done){
        done();
    })

    afterAll(async () => { 
        await Store.deleteMany().exec();
        await Product.deleteMany().exec();
        await disconnect();
    })
});