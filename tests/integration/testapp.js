const express = require("express");
const cors = require('cors');
const Product = require("../../api/models/Product");
const ProductAttribute  = require("../../api/models/ProductAttributes");
const Store  = require("../../api/models/Store");

const config = require('../../api/config');

const {connect} = require("../../api/db/connection");
connect(config.NODE_ENV_TEST);

const app = express();
app.use(cors());
app.use(express.json());

var productRouter = require('../../api/routes/productRoutes')(Product);
app.use("/api", productRouter);
var productAttributeRouter = require('../../api/routes/productAttributeRoutes')(ProductAttribute);
app.use("/api", productAttributeRouter);
var storeRouter = require('../../api/routes/storeRoutes')(Store);
app.use("/api", storeRouter);

module.exports = app;